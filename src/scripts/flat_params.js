let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find(".FilterSidebar_reset").on("click", (event) => {
    event.preventDefault();
    filter.resetFilterForm();
});

$(".FilterMobileSwitcher").selectmenu({
    change: (event, ui) => {
        window.location.href = ui.item.value;
    },
});

$('.PanelBtn-filter').on('click', function () {
    $('.FilterPage_overlay').toggleClass('FilterPage_overlay-active');
    $('body').toggleClass('scroll-lock');
    sidebar.toggle();
})

$('.FilterPage_overlay, .FilterSidebar_close, .FilterForm_btn-search').on('click', function () {
    $('.FilterPage_overlay').removeClass('FilterPage_overlay-active');
    $('body').toggleClass('scroll-lock');
    sidebar.toggle();
})

$(".FilterMobileSwitcher").selectmenu({
	change: (event, ui) => {
		window.location.href = ui.item.value;
	}
});


let flatsList = new FlatsList(".Tbl-flatParams", {
	renderElement: "#filterResultsContainer"
});

const LIMIT = 12;

let offset = 0;

let $getMoreFlatsBtn = $("#getMoreFlats");

let updateGetMoreBtn = response => {
	let flatsRemains = response.total - offset - LIMIT;
	let loadNextTimeCount = LIMIT;
	if (flatsRemains < LIMIT) {
		loadNextTimeCount = flatsRemains;
	}

	if (loadNextTimeCount <= 0) {
		$getMoreFlatsBtn.hide();
	} else {
		$getMoreFlatsBtn
			.show()
			.find("#extraFlatsCounter")
			.html(loadNextTimeCount);
	}
};

let filter = new FilterForm("#filterForm", {
	submitHandler: $filterForm => {
		// window.pagePreloader.show();
		offset = 0;
		let dataToSend = $filterForm.serializeObject();
		$.extend(true, dataToSend, {
			action: "get_flats",
			limit: LIMIT,
			offset: 0
		});

		$.ajax({
			url: $filterForm.attr("action") || $filterForm.data("action"),
			dataType: "json",
			method: "GET",
			data: dataToSend
		})
			.done(response => {
				flatsList.render({ flats: response.flats });
				updateGetMoreBtn(response);
			})
			.fail(() => {
				alert("Не удалось получить данные с сервера! Попробуйте позже.");
			})
			.always(function() {
				// window.pagePreloader.hide();
			});
	}
});

$getMoreFlatsBtn.on("click", event => {
	event.preventDefault();

	let dataToSend = filter.$filterForm.serializeObject();

	$.extend(true, dataToSend, {
		action: "get_flats",
		limit: LIMIT,
		offset: offset + LIMIT
	});

	$.ajax({
		url: filter.$filterForm.attr("action") || filter.$filterForm.data("action"),
		dataType: "json",
		method: "GET",
		data: dataToSend
	})
		.done(response => {
			flatsList.append({ flats: response.flats });
			offset += LIMIT;
			updateGetMoreBtn(response);
		})
		.fail(() => {
			alert("Не удалось получить данные с сервера! Попробуйте позже.");
		});
});

$('.FilterForm_label-accord').on('click', function () {
	$(this).toggleClass('FilterForm_label-accordActive');
	$(this).siblings('.FilterForm_chksOtherGroup').slideToggle(300);
});

$("#filterResult").on('click', function (event) {
    const $target = $(event.target);

    if ($target.hasClass('NoPlansFound_reset')) {
        event.preventDefault();

        $('.FilterSidebar_reset').trigger('click');
    }
});

let $sortInp = $("#sortInp");
let $sortOrderInp = $("#sortOrderInp");

let tbl = new Tbl(".Tbl-flatParams", {
	onSortChange: (sortName, direction) => {
		$sortInp.val(sortName);
		$sortOrderInp.val(direction).trigger("change");
	}
});


let popupTpl = _.template(
    `<div class="infoTooltip infoTooltip-flatParams" style="width: 222px; pointer-event:none;">
        <img class="infoTooltip_flatParamsImg" src="<%= img %>"/>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {

    let $scheme = $(".FilterPage_flatParamsList");

    if ($scheme.length) {
        $scheme.tooltip({
            items: "[data-thumb]",
            // track: true,
            // position: { my: "left+15 bottom", at: "right center" },
            hide: { duration: 0 },
            show: { duration: 0 },
            content: function() {
                let $el = $(this);
                let params = $el.data("thumb");
                let dataToRender = {
                    img: null
                };

                console.log(params)
                _.assignIn(dataToRender, params);
                return popupTpl(dataToRender, params);
            }
        });
    }
};

$(window).on("load", () => {
    setupSectionHoverBehaviour();
});