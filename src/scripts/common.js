/*=require ./includes/blocks/*.js*/
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/chunks/APP_CONFIGS.js */
let mainMenu = new MainMenu("#mainMenuModal");

$(".Header_menu").on("click", (event) => {
    event.preventDefault();
    mainMenu.show();
    return false;
});

$('.BtnUp').on('click', function () {

    $('body, html').animate({
        scrollTop: 0
    }, 500);
})

$('.LightboxImage').fancybox({});

function getWindowSizeBreakpoint() {
	let w = window.innerWidth;
	let result = "md";
	if (w < 768) {
		result = "xs";
	} else if (w > 767 && w < 1440) {
		result = "sm";
	}
	return result;
}

function prepareFilterFormValues(valsObj) {
    return _.mapValues(valsObj, (param) => {
        // convert all strings and arrays from inputs to 'INT' type
        let result;
        if (_.isString(param)) {
            result = parseInt(param);
        } else if (_.isArray(param)) {
            result = _.map(param, (p) => {
                return parseInt(p);
            });
        }
        return result;
    });
}

function saveFormState($form, storageID) {
    let savedJSON = localStorage.getItem(storageID);
    let valsToStore;
    if (savedJSON) {
        valsToStore = JSON.parse(savedJSON);
    } else {
        valsToStore = {};
    }

    let $inps = $form.find("[data-persist-id]");

    $inps.each(function(index, el) {
        let inpID = el.dataset.persistId;
        if (el.type && el.type === "checkbox") {
            valsToStore[inpID] = !!el.checked;
        } else if (el.tagName === "SELECT") {
            let tmpSelectedOptions = [];

            for (let i = el.options.length - 1; i >= 0; i--) {
                let opt = el.options[i];
                if (opt.value && opt.selected) {
                    tmpSelectedOptions.push(opt.value);
                }
            }

            valsToStore[inpID] = tmpSelectedOptions;
        } else {
            valsToStore[inpID] = el.value;
        }
    });

    localStorage.setItem(storageID, JSON.stringify(valsToStore));
    // console.log("saved form states:", valsToStore);
}

function restoreFormState($form, storageID) {
    let storedValues = JSON.parse(localStorage.getItem(storageID));
    if (!storedValues) return false;
    // console.log("restored form states:", storedValues);
    let $inps = $form.find("[data-persist-id]");

    $inps.each(function(index, el) {
        let inpID = el.dataset.persistId;
        if (el.type && el.type === "checkbox") {
            let isChecked = !!storedValues[el.dataset.persistId];
            el.checked = isChecked;
        } else if (el.tagName === "SELECT") {
            if (storedValues[inpID]) {
                let selectedOptsValues = storedValues[inpID];
                for (var i = 0; i < el.options.length; i++) {
                    let opt = el.options[i];
                    if (selectedOptsValues.includes(opt.value)) {
                        opt.selected = "selected";
                    }
                }
            }
        } else {
            el.value = storedValues[inpID];
        }
    });
}



new InteractiveForm($("#Callback").find(".Modal_form"));

$(".SelectPage")
      .selectmenu()