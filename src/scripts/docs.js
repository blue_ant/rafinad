var accordion = {
    init: function () {
        var $title = $('.Documents_name');
        var $content = $('.Documents_content');

        $content.hide();

        $title.on('click', function () {

            $content.not($(this).next()).slideUp(400).prev().removeClass('Documents_name-active');

            $(this).toggleClass('Documents_name-active').next().slideToggle(400);
        });
    }
};

accordion.init();

// TODO:
//  1) rework the clicks binding (resolve possible multiple bindings after resize)
//  2) window load event isn't realy needed
$(window).on('load resize', function () {

    if (window.matchMedia('(max-width: 767px)').matches) {
        var $nameDoc = $('.Documents_text');
        var $docDownload = $('.Documents_xsDownload');
        var $docUp = $('.Documents_xsDownloadUp');

        $docDownload.hide();

        $nameDoc.on('click', function () {
            $(this).siblings('.Documents_xsDownload').slideDown(300);
            $(this).slideUp(300);
        });

        $docUp.on('click', function () {
            $(this).parent().siblings('.Documents_text').slideDown(300);
            $(this).parent().slideUp(300);
        });
    }
});