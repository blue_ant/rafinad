let $infrasMap = $("#infrasMap");

const infrasMap = new GMaps({
    el: $infrasMap.get(0),
    center: {
        lat: window.centerCoords.lat,
        lng: window.centerCoords.lng,
    },
    disableDefaultUI: true,
    zoomControl: true,
    zoom: 16,
    styles: APP_CONFIGS.gmapsStyles,
});

if (window.infrasObjsMarkers) {
    window.infrasObjsMarkers = _.sortBy(window.infrasObjsMarkers, function(o) {
        return o.coord[0];
    });
    window.infrasObjsMarkers.forEach((item) => {
        new CustomMarker(infrasMap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover);
    });
}

$infrasMap.on('click', '.marker-hover', function() {
    let $el = $(this);
    if (!$el.hasClass('marker-active')) {
        $('.marker-active').removeClass('marker-active');
        $el.addClass('marker-active');
    } else {
        $el.removeClass('marker-active');
    }
});