class Promo {
	constructor(el) {
		this.$el = $(el);
		this.$foregroundCont = this.$el.find(".Promo_foregroundCont");
		this.$foregroundPic = this.$foregroundCont.find(".Promo_foregroundPic");
		this.$overlayField = this.$el.find(".Promo_foregroundPicOverlay");
		if (this.$foregroundPic.get(0).complete || this.$foregroundPic.get(0).readyState === 4) {
			this.fitForegroundPic();
		} else {
			this.$foregroundPic.on("load", () => {
				this.fitForegroundPic();
			});
		}

		$(window).on(
			"resize",
			_.debounce(() => {
				this.fitForegroundPic();
			}, 80)
		);

		this.$el.find(".Promo_link-hidePromo, .Promo_closeBtn").on("click", event => {
			event.preventDefault();
			this.hide();
		});
	}

	fitForegroundPic() {
		this.$overlayField.width(Math.ceil(this.$foregroundPic.width()));
		this.$overlayField.height(Math.ceil(this.$foregroundPic.height()));

		let ratio = this.$foregroundPic.width() / this.$foregroundPic.get(0).naturalWidth;
		TweenLite.set(this.$overlayField.find(".Promo_btnWrap"), {
			scale: ratio.toFixed(2)
		});
	}

	show() {
		this.$el.addClass("Promo-active");

		var borderWidth = 0;
		var reverse = false;
		var animation = function() {
			if (borderWidth === 8) {
				reverse = true;
			} else if (borderWidth === 0) {
				reverse = false;
			}

			borderWidth = reverse ? borderWidth - 1 : borderWidth + 1;

			$(".Promo_btn").css("border-width", `${borderWidth}px`);

			requestAnimationFrame(animation);
		};

		requestAnimationFrame(animation);
	}

	hide() {
		this.$el.removeClass("Promo-active");
	}
}
