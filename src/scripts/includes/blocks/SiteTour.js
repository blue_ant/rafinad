class SiteTour {
	constructor(modalElement, scripts = []) {
		this.isOpen = false;
		this.$modal = $(modalElement);
		this.currentStep = -1;
		this.scripts = scripts;
		this.$modal.css({
			margin: 0,
			position: "absolute",
			top: 0,
			left: 0,
			width: "100%",
			height: "100%"
		});

		this.$el = $("<div class='SiteTour'></div>");
		this.$shade1 = $("<div class='SiteTour_shade SiteTour_shade-n1'></div>");
		this.$shade2 = $("<div class='SiteTour_shade SiteTour_shade-n2'></div>");
		this.$shade3 = $("<div class='SiteTour_shade SiteTour_shade-n3'></div>");
		this.$shade4 = $("<div class='SiteTour_shade SiteTour_shade-n4'></div>");

		this.$messageBox = $("<div class='SiteTour_messageBox'></div>");
		this.$nextStepBtn = $("<a class='SiteTour_nextStepBtn' href='#'>Далее</a>");
		this.$counter = $("<span class='SiteTour_counter'>&nbsp;</span>");
		this.canvas = document.createElement("CANVAS");
		this.canvas.className = "SiteTour_canvas";
		this.$inner = $('<div class="SiteTour_inner"></div>');
		this.$closeBtn = $('<button class="SiteTour_closeBtn">&nbsp;</button>');
		this.$overlay = $('<div class="SiteTour_overlay"></div>');
		this.$overlay.append([this.$shade1, this.$shade2, this.$shade3, this.$shade4, this.canvas]).appendTo(this.$el);

		this.$inner.append([this.$closeBtn, this.$messageBox]);

		$('<div class="SiteTour_footer"></div>')
			.append([this.$nextStepBtn, this.$counter])
			.appendTo(this.$inner);

		this.$el.append(this.$inner);
		this.$modal.append(this.$el);

		this.$nextStepBtn.on("click", event => {
			event.preventDefault();
			this.nextStep();
		});

		this.$overlay
			.on("click", event => {
				event.preventDefault();
				if (event.target.className === "SiteTour_overlay") {
					this.hide();
				}
			})
			.on("touchstart", function(event) {
				event.preventDefault();
				event.stopPropagation();
				return false;
			});

		this.$closeBtn.on("click", event => {
			event.preventDefault();
			this.hide();
		});

		$(window).on(
			"resize",
			_.debounce(event => {
				if (this.isOpen) {
					event.preventDefault();
					this.renderStep(this.currentStep);
				}
			}, 100)
		);
	}

	/*getPageMaxScroll() {
		// Cross browser page height detection is ugly
		return (
			Math.max(
				document.body.scrollHeight,
				document.body.offsetHeight,
				document.documentElement.clientHeight,
				document.documentElement.scrollHeight,
				document.documentElement.offsetHeight
			) - window.innerHeight
		); // Subtract viewport height
	}*/

	renderStep(index) {
		if (!this.scripts[index]) {
			console.error("There is no step with index " + index);
			return false;
		}

		if (!this.isOpen) {
			this.show();
		}

		this.resetArrow();

		let $window = $(window);

		this.$counter.html(`${index + 1}/${this.scripts.length}`);
		this.$messageBox.html(this.scripts[index].message);

		if (!this.scripts[index].element) {
			this.resetOverlay();
		} else {
			let $target = $(this.scripts[index].element)
				.filter(":visible")
				.eq(0);

			if ($target.length == 0) {
				this.resetOverlay();
				console.error(
					`SiteTour highlight element (${this.scripts[index].element}) is not found or not visible`
				);
			}

			/*{
				let top = $target.offset().top;

				// Fix for bug on iOS devices
				// When top was larger than maximum page scroll
				// "getBoundingClientRect" would take that value into calculations
				const maxScroll = this.getPageMaxScroll();
				if (top > maxScroll) {
					top = maxScroll;
				}

				$window.scrollTop(top);
				this.lastScrollPosition = top;
			}*/

			let position = $target.get(0).getBoundingClientRect();

			let coords = {
				x1: position.left,
				x2: position.left + $target.width(),
				y1: position.top,
				y2: position.top + $target.height()
			};

			let ww = $window.width();
			let wh = $window.height();

			this.$shade1.css({
				top: 0,
				left: 0,
				right: 0,
				bottom: wh - coords.y1
			});

			this.$shade2.css({
				top: coords.y1,
				left: coords.x2,
				right: 0,
				bottom: wh - coords.y2
			});

			this.$shade3.css({
				top: coords.y2,
				left: 0,
				right: 0,
				bottom: 0
			});

			this.$shade4.css({
				top: coords.y1,
				left: 0,
				right: ww - coords.x1,
				bottom: wh - coords.y2
			});

			this.drawArrow(position, { ww, wh });
		}
	}

	nextStep() {
		this.currentStep += 1;

		if (this.currentStep >= this.scripts.length) {
			this.currentStep = 0;
		}

		this.renderStep(this.currentStep);
	}

	drawArrow(targetRect, windowDimensions) {
		this.$inner.removeAttr("style");

		let canvas = this.canvas;
		let ww = windowDimensions.ww;
		let wh = windowDimensions.wh;

		canvas.width = ww;
		canvas.height = wh;

		let elPosition = {
			isTopLeft: false,
			isTopRight: false,
			isBottomLeft: false,
			isBottomRight: false
		};

		let centerOfTarget = {
			y: targetRect.top + targetRect.height / 2,
			x: targetRect.left + targetRect.width / 2
		};

		{
			let onLeft = centerOfTarget.x < ww / 2;
			let onTop = centerOfTarget.y < wh / 2;

			if (onTop && onLeft) {
				elPosition.isTopLeft = true;
			} else if (onTop && !onLeft) {
				elPosition.isTopRight = true;
			} else if (!onTop && onLeft) {
				elPosition.isBottomLeft = true;
			} else if (!onTop && !onLeft) {
				elPosition.isBottomRight = true;
			}
		}

		let textBoxRect = this.$messageBox.get(0).getBoundingClientRect();

		if (elPosition.isTopRight || elPosition.isTopLeft) {
			let isElOverMessage = targetRect.bottom > textBoxRect.top - 54;
			if (isElOverMessage) {
				this.$inner.css("top", `${targetRect.bottom - textBoxRect.top + 54}px`);
			}
		} else if (elPosition.isBottomRight || elPosition.isBottomLeft) {
			let isElOverMessage = targetRect.top < textBoxRect.bottom + 54;
			if (isElOverMessage) {
				this.$inner.css("top", `${targetRect.top - textBoxRect.bottom - 54}px`);
			}
		}

		textBoxRect = this.$messageBox.get(0).getBoundingClientRect();

		let startPoint, turnPoint, endPoint, firstPinPoint, secondPinPoint;

		if (elPosition.isTopRight) {
			startPoint = {
				x: textBoxRect.right - 1,
				y: parseInt(textBoxRect.top + textBoxRect.height / 2)
			};

			turnPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: textBoxRect.top + textBoxRect.height / 2
			};

			endPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: targetRect.bottom + 20
			};

			firstPinPoint = {
				x: endPoint.x - 9,
				y: endPoint.y + 12
			};

			secondPinPoint = {
				x: endPoint.x + 9,
				y: endPoint.y + 12
			};
		} else if (elPosition.isTopLeft) {
			startPoint = {
				x: textBoxRect.left + 1,
				y: parseInt(textBoxRect.top + textBoxRect.height / 2)
			};

			turnPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: textBoxRect.top + textBoxRect.height / 2
			};

			endPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: targetRect.bottom + 20
			};

			firstPinPoint = {
				x: endPoint.x - 9,
				y: endPoint.y + 12
			};

			secondPinPoint = {
				x: endPoint.x + 9,
				y: endPoint.y + 12
			};
		} else if (elPosition.isBottomLeft) {
			startPoint = {
				x: textBoxRect.left + 1,
				y: parseInt(textBoxRect.top + textBoxRect.height / 2)
			};

			turnPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: textBoxRect.top + textBoxRect.height / 2
			};

			endPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: targetRect.top - 20
			};

			firstPinPoint = {
				x: endPoint.x - 9,
				y: endPoint.y - 12
			};

			secondPinPoint = {
				x: endPoint.x + 9,
				y: endPoint.y - 12
			};
		} else if (elPosition.isBottomRight) {
			startPoint = {
				x: textBoxRect.right - 1,
				y: parseInt(textBoxRect.top + textBoxRect.height / 2)
			};

			turnPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: textBoxRect.top + textBoxRect.height / 2
			};

			endPoint = {
				x: targetRect.left + targetRect.width / 2,
				y: targetRect.top - 20
			};

			firstPinPoint = {
				x: endPoint.x - 9,
				y: endPoint.y - 12
			};

			secondPinPoint = {
				x: endPoint.x + 9,
				y: endPoint.y - 12
			};
		}

		let isLineSuitable = true;

		if (turnPoint.x > textBoxRect.left - 17 && turnPoint.x < textBoxRect.right + 17) {
			isLineSuitable = false;
		}

		if (centerOfTarget.y > textBoxRect.top - 54 && centerOfTarget.y < textBoxRect.bottom + 54) {
			isLineSuitable = false;
		}

		if (isLineSuitable) {
			let ctx = canvas.getContext("2d");
			ctx.beginPath();
			ctx.moveTo(startPoint.x, startPoint.y);
			ctx.lineTo(turnPoint.x, turnPoint.y);
			ctx.lineTo(endPoint.x, endPoint.y);
			ctx.moveTo(endPoint.x, endPoint.y - 1);
			ctx.lineTo(firstPinPoint.x, firstPinPoint.y);
			ctx.moveTo(endPoint.x, endPoint.y - 1);
			ctx.lineTo(secondPinPoint.x, secondPinPoint.y);
			ctx.strokeStyle = "#d8d1c2";
			ctx.stroke();
		}
	}

	resetArrow() {
		let ctx = this.canvas.getContext("2d");
		ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	resetOverlay() {
		[this.$shade1, this.$shade2, this.$shade3].forEach($shd => {
			$shd.css({
				top: 0,
				left: 0,
				right: "100%",
				bottom: "100%"
			});
		});

		this.$shade4.css({
			top: 0,
			left: 0,
			right: 0,
			bottom: 0
		});
	}

	show() {
		this.isOpen = true;
		$.fancybox.open(this.$modal, {
			slideClass: "fancybox-slide--sitetour",
			touch: false,
			modal: true,
			smallBtn: false,
			backFocus: false,
			baseTpl:
				'<div class="fancybox-container" role="dialog" tabindex="-1">' +
				'<div class="fancybox-inner">' +
				'<div class="fancybox-stage"></div>' +
				"</div>" +
				"</div>"
		});
	}

	hide() {
		$.fancybox.close();
		this.isOpen = false;
	}
}
