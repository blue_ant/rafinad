class FilterSidebar {
    constructor(el) {
        let $el = $(el);

        if ($el.length === 0) {
            console.error("FilterSidebar constructor can\'t find given \"el\" in DOM!");
            return;
        }

        
        /*const scrollerInst = */
        new PerfectScrollbar($el.find('.FilterForm').get(0), {
            suppressScrollX: true,
            swipeEasing: false,
        });

        $el.find('.FilterSidebar_grip').on('click', (event) => {
            event.preventDefault();
            this.toggle();
        });

        
        this.$el = $el;
    }

    show() {
        this.$el.addClass('FilterSidebar-active');
        // this.$headerLogoEl.addClass('Header_logo-notShadowed');
    }

    hide() {
        this.$el.removeClass('FilterSidebar-active');
        // this.$headerLogoEl.removeClass('Header_logo-notShadowed');
    }

    toggle() {
        if (this.$el.hasClass('FilterSidebar-active')) {
            this.hide();
        } else {
            this.show();
        }
    }

    addToggler(selector) {
        $(selector).on('click', (event) => {
            event.preventDefault();
            this.toggle();
        });
    }
}