class FlatsList {
	constructor(el, opts) {
		this.$el = $(el);
		this.$renderEl = $(opts.renderElement);

		let tplStr = `
//=require ../chunks/FlatRow.tpl
`;
		this.tpl = _.template(tplStr.replace(/\s{2,}/g, ""));
	}

	render(dataToRender) {
		let markup = this.tpl(dataToRender);
		this.$renderEl.html(markup);
	}

	append(dataToRender) {
		let markup = this.tpl(dataToRender);
		this.$renderEl.append(markup);
	}
}