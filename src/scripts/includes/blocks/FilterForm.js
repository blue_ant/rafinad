class FilterForm {
    constructor(el, opts={
        submitHandler: $.noop,
    }) {

        if (!el) {
            console.error('Filter class constructor requires "el" argument!');
            return;
        }

        

        const submitHandler = _.debounce(opts.submitHandler, 700);

        let $filterForm = $(el);

        restoreFormState($filterForm, "serebricaFilterFormState");

        if (!$filterForm.length) {
            console.error('FilterForm constructor can\'t find given "el" in DOM!');
            return;
        }


        let multicheckboxes = [];
        $filterForm.find(".Multycheckbox").each((index, el) => {
            multicheckboxes.push(new Multicheckbox(el));
        });

        let rangeSliders = [];
        $filterForm.find(".RangeSlider").each((index, el)=> {
            rangeSliders.push(
                new RangeSlider(el)
            );
        });

        let $inputs = $($filterForm.get(0).elements);

        $inputs.on('change', (event) => {
            event.preventDefault();
            $filterForm.trigger('submit');
        });

        $filterForm.on('submit', (event) => {
            event.preventDefault();
            submitHandler($filterForm);
            saveFormState($filterForm, "serebricaFilterFormState");
            return false;
        }).trigger('submit');


        $(".FiltersOpts_resetBtn").on('click', () => {
            this.resetFilterForm();
        });

        this.$filterForm = $filterForm;
        this.$inputs = $inputs;
        this.rangeSliders = rangeSliders;
        this.multicheckboxes = multicheckboxes;
    }

    resetFilterForm() {
        this.$filterForm[0].reset();
        this.rangeSliders.forEach((rangeSliderInst) => {
            rangeSliderInst.reset();
        });

        this.multicheckboxes.forEach((multiCheckboxInst) => {
            multiCheckboxInst.reset();
        });

    }

    /*save() {
        
    }

    restore() {
        
    }*/
}