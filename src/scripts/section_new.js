
let popupTpl = _.template(
    `<div class="Dialog" style="max-width: 400px; pointer-event:none;">
        <div class="Dialog_head"><%= title %></div>
        <div class="Dialog_content">
            <div class="Dialog_num">Квартира № <%= num %></div>
            <dl class="Dialog_dl">
                <dt class="Dialog_dt">Корпус:</dt>
                <dd class="Dialog_dd"><%= house %></dd>
                <dt class="Dialog_dt">Этаж:</dt>
                <dd class="Dialog_dd"><%= floor %></dd>
                <dt class="Dialog_dt">Общая площадь:</dt>
                <dd class="Dialog_dd"><%= area %> м<sup>2</sup></dd>
                <dt class="Dialog_dt">Стоимость:</dt>
                <dd class="Dialog_dd"><%= price.toLocaleString("Ru-ru") %>&nbsp;₽</dd>
            </dl>
        </div>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {
    $(".BldScheme_link").hover(
        event => {
            let $item = $(event.currentTarget);
            $item.addClass("BldScheme_link-active");
        },
        event => {
            let $item = $(event.currentTarget);
            $item.removeClass("BldScheme_link-active");
        }
    );

    let $scheme = $(".BldScheme");

    if ($scheme.length) {
        $scheme.tooltip({
            items: "[data-params]",
            track: true,
            hide: { duration: 0 },
            show: { duration: 0 },
            content: function() {
                let $el = $(this);
                let params = $el.data("params");
                let dataToRender = {
                    title: null,
                    num: null,
                    price: null,
                    house: null,
                    floor: null,
                    area: null
                };
                _.assignIn(dataToRender, params);
                return popupTpl(dataToRender);
            }
        });
    }
};

$(window).on("load", () => {
    setupSectionHoverBehaviour();
});


$('.SelectFloor').selectmenu({
    classes: {
        "ui-selectmenu-menu": "SelectFloor",
        "ui-selectmenu-text": "SelectFloor_text",
        "ui-selectmenu-button": "SelectFloor_btn"
    },
    change: function( event, ui ) {
        console.log('ui.item.value', ui.item.value)
        let ref = ui.item.value;
        let $schemeBlock = $(".HouseSection_schemeScroll");
        $schemeBlock.addClass("HouseSection_inner-disabled");
        $.ajax({
            url: ref,
            dataType: "html"
        })
            .done(response => {
                let $newDoc = $(response);

                let $newSectionInfo = $newDoc.find(".HouseSection_schemeScroll");
                $schemeBlock.html($newSectionInfo.html());
                setupSectionHoverBehaviour();

                let title = $newDoc.filter("title").html();
                document.title = title;
                history.replaceState({}, title, ref);
            })
            .fail(function() {
                alert("Не удалось получить данные о секции!/nПопробуйте позже.");
            })
            .always(function() {
                $schemeBlock.removeClass("HouseSection_inner-disabled");
            });
    }
});

$("main").on("click", ".HouseSectionNav_switch", event => {
    event.preventDefault();
    let currentLink = event.currentTarget;
    let ref = event.currentTarget.href;
    $('.HouseSectionNav_link').removeClass('HouseSectionNav_link-active');
    currentLink.className += ' HouseSectionNav_link-active';
    let $schemeBlock = $(".HouseSection_schemeScroll");
    $schemeBlock.addClass("HouseSection_inner-disabled");
    $.ajax({
        url: ref,
        dataType: "html"
    })
        .done(response => {
            let $newDoc = $(response);

            let $newSectionInfo = $newDoc.find(".HouseSection_schemeScroll");
            $schemeBlock.html($newSectionInfo.html());
            setupSectionHoverBehaviour();

            let title = $newDoc.filter("title").html();
            document.title = title;
            history.replaceState({}, title, ref);
        })
        .fail(function() {
            alert("Не удалось получить данные о секции!/nПопробуйте позже.");
        })
        .always(function() {
            $schemeBlock.removeClass("HouseSection_inner-disabled");
        });
});

{
    let activeHouse = $('.HousesNav_item-active').data('name');
    //$('.HousesScheme_svg').find('[data-name="'+ activeHouse +'"]').addClass('HousesScheme_polygonActive');
}

