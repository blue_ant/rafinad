let sidebar = new FilterSidebar("#filterSidebar");

sidebar.$el.find(".FilterSidebar_reset").on("click", (event) => {
    event.preventDefault();
    filter.resetFilterForm();
});

$(".FilterMobileSwitcher").selectmenu({
    change: (event, ui) => {
        window.location.href = ui.item.value;
    },
});

$('.PanelBtn-filter').on('click', function () {
    $('.FilterPage_overlay').toggleClass('FilterPage_overlay-active');
    $('body').toggleClass('scroll-lock');
    sidebar.toggle();
})

$('.FilterPage_overlay, .FilterSidebar_close, .FilterForm_btn-search').on('click', function () {
    $('.FilterPage_overlay').removeClass('FilterPage_overlay-active');
    $('body').toggleClass('scroll-lock');
    sidebar.toggle();
})

let cardTplStr = `
/*=require ./includes/chunks/FlatCard.tpl */
`;

let plansListBuilder = new CardsList({
    tpl: cardTplStr,
    additionalClassName: "CardsList-flatPlans",
});

let filter = new FilterForm("#filterForm", {
    submitHandler: ($filterForm) => {
        // console.log(" --- submitHandler fired---");
        let currentFiltersParams = $filterForm.serializeObject();

        $.ajax({
            url: $filterForm.attr("action") || $filterForm.data("action"),
            dataType: "json",
            method: "GET",
            // localCache: false,
            // cacheTTL: 1,
            // cacheKey: 'typicalsJSONCache',
            data: currentFiltersParams,
        })
            .done((jsonResponse) => {
                // parse filters params to object
                console.log(currentFiltersParams);

                if (jsonResponse.floor_num) {
                    console.log("floor ..");
                    var ids = jsonResponse.floor_num;
                    var floor_cnt = jsonResponse.floor_cnt;


                    console.log( floor_cnt );

                    $(".Genplan_poligonLink ").each(function(){
                        var flat_id = parseInt( $(this).data("floor_num") );
                        var ddata = JSON.parse('{"floor": "'+ flat_id +'", "flats": "'+ ( (floor_cnt[ flat_id ]) ? floor_cnt[ flat_id ] : 0 ) +'"}');

                        $(this).data('floor', ddata).prop('data-floor', ddata).attr('data-floor', ddata)

                        if ( ids.indexOf( flat_id ) >= 0 ) {
                            if ( !$(this).hasClass("Genplan_poligonLink-filtered")  )
                                $(this).addClass("Genplan_poligonLink-filtered");
                        } else
                            $(this).removeClass("Genplan_poligonLink-filtered");
                    });

                    var $scheme = $(".HouseFloor");


                    //initTitle();


                    var $title = $scheme.find('[data-floor]');
                    var popupTpl = _.template('<div class="infoTooltip" style="max-width: 164px; pointer-event:none;">\n\t\t\t\t\t<div class="infoTooltip_title"> <%= floor %> \u044D\u0442\u0430\u0436</div>\n\t\t\t\t\t<div class="infoTooltip_content"> <%= flats %> \u043A\u0432\u0430\u0440\u0442\u0438\u0440 </div>\n\t\t\t\t</div>'.replace(/\s{2,}/g, ""));

                    $scheme.tooltip({
                        items: "[data-floor]",
                        track: true,
                        hide: { duration: 0 },
                        show: { duration: 0 },
                        content: function content() {
                            var $el = $(this);
                            var params = $el.data("floor");
                            var dataToRender = {
                                num: null,
                                flats: null
                            };

                            console.log(params);

                            _.assignIn(dataToRender, params);
                            return popupTpl(dataToRender, params);
                        }
                    });

                    return;
                } else
                if ( jsonResponse.flat_ids ) {
                    console.log( "floor flats.." );

                    var ids = jsonResponse.flat_ids;

                    $(".BldScheme_link").each(function(){
                        var flat_id = parseInt( $(this).attr("flat_id") );
                        if ( ids.indexOf( flat_id ) >= 0 ) {
                            $(this).show();
                        } else
                            $(this).hide();
                    });

                    return;
                }


				let filteredPlans = jsonResponse.objs;
                filteredPlans = _.each(filteredPlans, function (pln) {
                    const flats = pln.flats;
                    let areaMin = 0;
                    let priceMin = 0;

                    _.each(flats, (flat) => {
                        if (areaMin < flat.area) {
                            areaMin = flat.area;
                        }

                        if (priceMin < flat.price) {
                            priceMin = flat.price;
                        }
                    })

                    pln.areaMin = areaMin;
                    pln.priceMin = priceMin;

                });

                {
                    let urlSuffix = $.param(currentFiltersParams);

                    _.forEach(filteredPlans, plan => {
                        plan.href += "?" + urlSuffix;
                    });
                }

                
                filteredPlans = _.groupBy(filteredPlans, "rooms");
                
                if (!Object.keys(filteredPlans).length) {
					$("#filterResult").html(
                        `<div class="NoPlansFound">
                            <div class="NoPlansFound_wrap">
                                <img class="NoPlansFound_img" src="/img/ui/not-found.png" alt=""/>
                                <p class="NoPlansFound_text">Планировки по заданным параметрам не найдены,<br class="hidden-xs"> пожалуйста измените параметры поиска.</p>
                                <a href="#" class="NoPlansFound_reset">Сбросить фильтр</a>
                            </div>
                        </div>`
					);
                } else {
                    let dataToRender = {
                        plans: filteredPlans,
                        promos: jsonResponse.promos
                    };
    
                    $("#filterResult").html(plansListBuilder.renderCards(dataToRender));
                }
                
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});

$('.FilterForm_label-accord').on('click', function () {
	$(this).toggleClass('FilterForm_label-accordActive');
	$(this).siblings('.FilterForm_chksOtherGroup').slideToggle(300);
});

$("#filterResult").on('click', function (event) {
    const $target = $(event.target);

    if ($target.hasClass('NoPlansFound_reset')) {
        event.preventDefault();

        $('.FilterSidebar_reset').trigger('click');
    }
});