let thumbSlider = new Swiper($(".FlatSingle_planThumb .swiper-container"), {
    slidesPerView: "auto",
    slideToClickedSlide: true,
    navigation: {
        nextEl: ".FlatSingle_planThumbBtn-next",
        prevEl: ".FlatSingle_planThumbBtn-prev",
    },
    breakpoints: {
        767: {
            slidesPerView: 1,
            centeredSlides: true,
        },
    },
    on: {
        init: function() {
            this.slides[0].classList.add("FlatSingle_planThumbItem-active");
            if (this.slides.length <= 3) {
                $(".FlatSingle_planThumbBtn").css("display", "none");
            }
        },
        click: function() {
            let clickedSlide = this.clickedSlide;
            if (clickedSlide) {
                $.each(this.slides, (key, schemeSlide) => {
                    schemeSlide.classList.remove("FlatSingle_planThumbItem-active");
                    if (schemeSlide === clickedSlide) {
                        schemeSlider.slideTo(key, 400, false);
                        this.slideTo(key, 400, false);
                    }
                });
                clickedSlide.classList.add("FlatSingle_planThumbItem-active");
            }
        },
    },
});

let schemeSlider = new Swiper($(".FlatSingle_slider"), {
    slidesPerView: 1,
    effect: "fade",
    spaceBetween: 30,
    on: {
        slideChange: function() {
            let main = thumbSlider.slides[schemeSlider.activeIndex];
            $.each(thumbSlider.slides, (key, images) => {
                images.classList.remove("FlatSingle_planThumbItem-active");
                if (images === main) {
                    schemeSlider.slideTo(key, 400, false);
                    thumbSlider.slideTo(key, 400, false);
                }
            });
            main.classList.add("FlatSingle_planThumbItem-active");
        },
        init: function() {
            if (this.slides.length < 2) {
                $(".FlatSingle_planThumb").css("display", "none");
                this.allowSlidePrev = false;
                this.allowSlideNext = false;
            }
        },
    },
});

let previewSlider = new Swiper($(".FlatSingle_preview .swiper-container"), {
    slidesPerView: 3,
    navigation: {
        nextEl: ".FlatSingle_previewBtn-next",
        prevEl: ".FlatSingle_previewBtn-prev",
    },
    spaceBetween: 0,
    breakpoints: {
        1439: {
            spaceBetween: 0,
        },
        767: {
            spaceBetween: 0,
        },
    },
});

if (previewSlider.slides && previewSlider.slides.length < 4) {
    previewSlider.allowTouchMove = false;
}

$("#scrollToFootageLink").on("click", (event) => {
    event.preventDefault();
    $("html, body").animate({ scrollTop: $("#footageBlock").offset().top }, 600);
    return false;
});

{
    let $popupLink = $("#showFootagePopupLink");
    let $popContent = $("#footageBlock")
        .find(".Panel")
        .clone();

    $popContent.addClass("Panel-popup").css("min-width", "464px");

    $popupLink
        .on("click", function(event) {
            event.preventDefault();
            return false;
        })
        .tooltip({
            items: $popupLink,
            position: {
                my: "center top",
                at: "center bottom+18",
                of: $popupLink,
            },
            content: $popContent,
            show: 200,
            hide: 200,
            tooltipClass: "ui-tooltip-auto-width",
        });
}
