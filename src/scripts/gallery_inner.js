new InlineGallery($('.gallery'), $('.js-modal'));

var galleryInfo = {

    init: function () {
        if ($('.gallery_infoIcon').length) {
            
            $('.gallery_info').slideUp(0);

            $('.gallery_infoIcon').on('click', function () {
                $(this).siblings('.gallery_info').slideToggle(200);
            });
        }
    }
}

galleryInfo.init();