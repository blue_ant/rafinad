new Fitblock(document.querySelector(".Fitblock"));

{
    let activeHouse = $('.HousesNav_item-active').data('name');
    //$('.HousesScheme_svg').find('[data-name="'+ activeHouse +'"]').addClass('HousesScheme_polygonActive');
}

let setupSectionHoverBehaviour = () => {
	if ($(".HouseFloor").length) {
		var $scheme = $(".HouseFloor");

		initTitle();

		function initTitle() {
			var $title = $scheme.find('[data-floor]');
			var popupTpl = _.template(
				`<div class="infoTooltip" style="max-width: 164px; pointer-event:none;">
					<div class="infoTooltip_title"> <%= floor %> этаж</div>
					<div class="infoTooltip_content"> <%= flats %> квартир </div>
				</div>`.replace(/\s{2,}/g, "")
			);

			$scheme.tooltip({
				items: "[data-floor]",
				track: true,
				hide: { duration: 0 },
				show: { duration: 0 },
				content: function() {
					let $el = $(this);
					let params = $el.data("floor");
					let dataToRender = {
						num: null,
						flats: null
					};

					console.log(params);

					_.assignIn(dataToRender, params);
					return popupTpl(dataToRender, params);
				}
			});
		}
 
	}
};

$(window).on("load", () => {
	setupSectionHoverBehaviour();
});
