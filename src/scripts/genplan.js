new Fitblock(document.querySelector(".Fitblock"));


let popupTpl = _.template(
    `<div class="infoTooltip infoTooltip-genplan" style="width: 270px; pointer-event:none;">
        <div class="infoTooltip_title"> Корпус <%= corpus %> <span class="infoTooltip_genplanSumRooms"><%= fullRooms %> квартир</span></div>
        <div class="infoTooltip_content">
            <div class="infoTooltip_genplanRow">
                <div class="infoTooltip_genplanName">1-ая квартира</div><div class="infoTooltip_genplanNum"><%= room1 %></div>
            </div>
            <div class="infoTooltip_genplanRow">
                <div class="infoTooltip_genplanName">2-ая квартира</div><div class="infoTooltip_genplanNum"><%= room2 %></div>
            </div>
            <div class="infoTooltip_genplanRow">
                <div class="infoTooltip_genplanName">3-ая квартира</div><div class="infoTooltip_genplanNum"><%= room3 %></div>
            </div>
            <div class="infoTooltip_genplanRow">
                <div class="infoTooltip_genplanName">3-ая квартира (евро)</div><div class="infoTooltip_genplanNum"><%= eroom3 %></div>
            </div>
        </div>
    </div>`.replace(/\s{2,}/g, "")
);

let setupSectionHoverBehaviour = () => {
    let $scheme = $(".BuildMap-genplan");

    if ($scheme.length) {
        $scheme.tooltip({
            items: "[data-building]",
            track: true,
            hide: { duration: 0 },
            show: { duration: 0 },
            content: function() {
                let $el = $(this);
                let params = $el.data("building");
                let dataToRender = {
                    room: null,
                    flats: null
                };

                console.log(params)
                _.assignIn(dataToRender, params);
                return popupTpl(dataToRender, params);
            }
        });
    }
};

$(window).on("load", () => {
    setupSectionHoverBehaviour();
});