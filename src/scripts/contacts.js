const accordion = $(".contacts .Accordion");

// new Accordion(accordion, {
//     collapsible: true,
//     active: false,
// });
$('.contacts_accordionContentWrp').each(function () {
	let ps = new PerfectScrollbar($(this)[0], {
		suppressScrollX: true,
		maxScrollbarLength: 25,
	});
});


const contactMap = new ContactMaps(new google.maps.LatLng(window.officeCoords.lat, window.officeCoords.lng), window.routesDesctiptions, accordion);

$('.Panel_control-contacts').on('click', function () {

	var dataType = this.dataset.type;
	var panelTitle = $('.contacts_accordionLabel');
	var panelContent = $('.contacts_accordionContent');
	var label = $('.contacts_accordionLabel[data-type="' + dataType + '"]');

	label.toggleClass('contacts_accordionLabel-visible')
	label.next().toggleClass('contacts_accordionContent-visible');

	label.siblings('.contacts_accordionLabel-visible').removeClass('contacts_accordionLabel-visible');
	label.next().siblings('.contacts_accordionContent-visible').removeClass('contacts_accordionContent-visible');

	if (this.dataset.type == 'office') {
		contactMap.hideRoute();
		contactMap.setCenterOffice();
	}
	if (this.dataset.type == 'mcd') {
		$('.McdMap').addClass('McdMap-open');
	}
	else {
		contactMap.showRoute(this.dataset.type);
	}
});

$('.McdMap_close').click(function(event) {
	$('.McdMap').removeClass('McdMap-open');
});