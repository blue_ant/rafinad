let tourInst = new SiteTour("#siteTourModal", [
    {
        element: "#showRoutesListBtn, .Transport_mobileControl-routes",
        message: "Кликните на интересующий вас маршрут"
    },
    {
        element: "#showCarRouteOnBigMapBtn, .Transport_mobileControl-dest",
        message: "Кликните на &laquo;маршрут&raquo; для того что бы построить маршрут из любой точки"
    }
]);

$(document).ready(function() {
    if ( window.is_transport )
        tourInst.nextStep();
});
let $transportMap = $("#transportMap");

let mapParams = {
    el: $transportMap.get(0),
    center: APP_CONFIGS.buildingLatLng,
    disableDefaultUI: true,
    zoomControl: true,
    zoom: init_zoom,
    styles: APP_CONFIGS.gmapsStyles
};

let windowBreakpoint = getWindowSizeBreakpoint();

if (windowBreakpoint === "md") {
    mapParams.center = {
        lat: APP_CONFIGS.buildingLatLng.lat,
        lng: APP_CONFIGS.buildingLatLng.lng + 0.0075
    };
} else if (windowBreakpoint === "sm") {
    mapParams.zoom = 14;
    mapParams.center = {
        lat: APP_CONFIGS.buildingLatLng.lat + 0.003,
        lng: APP_CONFIGS.buildingLatLng.lng + 0.009
    };
} else if (windowBreakpoint === "xs") {
    mapParams.center = {
        lat: APP_CONFIGS.buildingLatLng.lat + 0.0033,
        lng: APP_CONFIGS.buildingLatLng.lng + 0.006
    };
    mapParams.zoom = 14;
}

if ( window.is_transport )
    mapParams.zoom = init_zoom;

const transportMap = new GMaps(mapParams);

{
    var path = [
        [55.957113, 37.447534],
        [55.959647, 37.452028],
        [55.958398, 37.454493],
        [55.95665, 37.451858],
        [55.956303, 37.448081],
        [55.957113, 37.447534]
        // [55.956900, 37.447320],
    ];

    transportMap.drawPolygon({
        paths: path,
        strokeColor: "#251914",
        strokeOpacity: 1,
        strokeWeight: 2,
        fillColor: "#251914",
        fillOpacity: 0
    });
}

if (window.infrasObjsMarkers) {
    window.infrasObjsMarkers = _.sortBy(window.infrasObjsMarkers, function(o) {
        return o.coord[0];
    });
    window.infrasObjsMarkers.forEach(item => {
        new CustomMarker(transportMap.map, item.img, item.title, new google.maps.LatLng(...item.coord), item.hover);
    });
}

$transportMap.on("click", ".marker-hover", function() {
    let $el = $(this);
    if (!$el.hasClass("marker-active")) {
        $(".marker-active").removeClass("marker-active");
        $el.addClass("marker-active");
    } else {
        $el.removeClass("marker-active");
    }
});

let messageBox = new Message(".Message", {
    beforeHide: () => {
        transportMap.cleanRoute();

        let optimalZoom = transportMap.getZoom() + 1;
        if (optimalZoom > 16) {
            optimalZoom = 16;
        }
        transportMap.setCenter(APP_CONFIGS.buildingLatLng);
        transportMap.setZoom(optimalZoom);
    }
});

$("#showRoutesListBtn").on("click", function() {
    $(".Transport_routes").toggle();
    $(".Transport_routes").toggleClass("isOpen");

    if ($(".Transport_routes").hasClass("isOpen")) {
        $(".Message_dialog").hide();
    } else {
        $(".Message_dialog").show();
    }

    directionsDisplay.setMap(null);
    if (customMarker) {
        customMarker.setMap(null);
    }
    $directionPopup.hide();
    google.maps.event.clearListeners(transportMap.map, "click");
});

$(document).ready(() => {
    if (window.matchMedia("(min-width: 1279px)").matches) {
        console.log('matches');

        var fshow = true;

        if ( window.hide_right_menu != undefined ) {
            if ( window.hide_right_menu )
                fshow = false;
        }

        console.log( "hide_right_menu=" + hide_right_menu );
        console.log( "fshow=" + fshow );

        if ( fshow )
            $("#showRoutesListBtn").trigger('click');
    } else {
        $(".Transport_routes").hide();
    }
});

$("#listsOfRoutes")
    .find("a")
    .on("click", function(event) {
        event.preventDefault();
        let $link = $(event.currentTarget);
        let routeIndex = parseInt($link.attr("href").split("#route_")[1]);
        let routeData = window.routesDesctiptions[routeIndex];
        let routeColor = $link.attr("data-color");

        transportMap.cleanRoute();

        let dest = routeData.destination || [APP_CONFIGS.buildingLatLng.lat, APP_CONFIGS.buildingLatLng.lng];
        let tm = routeData.travelMode || "driving";
        let waypts = [];
        if (routeData.waypoints && tm !== "transit") {
            for (var i = routeData.waypoints.length - 1; i >= 0; i--) {
                waypts.push({
                    location: routeData.waypoints[i]
                });
            }
        }

        // выводим маршрут следования, интервалами
        for (var ii = 1; ii < window.routesDesctiptions2[routeIndex].waypoints.length; ii++) {
            let start = window.routesDesctiptions2[routeIndex].waypoints[ii - 1];
            let finish = window.routesDesctiptions2[routeIndex].waypoints[ii];

            if (finish.mode == "WALKING") finish.mode = "walk";
            if (finish.mode == "TRANSIT") finish.mode = "transit";
            if (finish.mode == "DRIVING") finish.mode = "driving";

            if (finish.mode == "walk") {
                transportMap.drawRoute({
                    origin: [start.lat, start.lng],
                    destination: [finish.lat, finish.lng],
                    strokeOpacity: 0,
                    icons: [
                        {
                            icon: {
                                path: "M 0,-1 0,1",
                                strokeOpacity: 1,
                                strokeColor: "#78912a",
                                strokeWeight: 3,
                                scale: 4
                            },
                            offset: "0",
                            repeat: "25px"
                        }
                    ],
                    // waypoints: [],
                    travelMode: finish.mode
                });
            } else {
                transportMap.drawRoute({
                    origin: [start.lat, start.lng],
                    destination: [finish.lat, finish.lng],
                    strokeColor: routeColor || "#78912a",
                    strokeOpacity: 1,
                    strokeWeight: 3,
                    // waypoints: [],
                    travelMode: finish.mode
                });
            }
        }

        transportMap.fitLatLngBounds([
            {
                lat: routeData.origin[0],
                lng: routeData.origin[1]
            },
            {
                lat: dest[0],
                lng: dest[1]
            }
        ]);

        messageBox.setContent(routeData);
        messageBox.show();
        $("#showRoutesListBtn").trigger("click");
    });

var $directionPopup = $(".TransportPopup");

var directionsDisplay = new google.maps.DirectionsRenderer({
    polylineOptions: {
        strokeColor: "#78912a"
    },
    suppressMarkers: true
});
directionsDisplay.setMap(transportMap.map);
var directionsService = new google.maps.DirectionsService();

var customMarker = null;

function bindMapRouteRenderOnClick(mapInstance, travelMode) {
    directionsDisplay.setMap(null);
    google.maps.event.clearListeners(mapInstance, "click");
    mapInstance.setOptions({
        draggableCursor: "pointer"
    });
    mapInstance.addListener("click", function(event) {
        if (customMarker) {
            customMarker.setMap(null);
        }

        let request = {
            origin: event.latLng,
            destination: APP_CONFIGS.buildingLatLng,
            travelMode: google.maps.TravelMode[travelMode]
        };
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(mapInstance);

                const markerTpl = `<div class="MarkerTpl"><div class="MarkerTpl_title">Время в пути </div><div class="MarkerTpl_time"> ${
                    response.routes[0].legs[0].duration.text
                }</div></div>`;

                var infowindow = new google.maps.InfoWindow({
                    content: markerTpl,
                    maxWidth: 50
                });

                customMarker = new google.maps.Marker({
                    position: response.routes[0].legs[0].start_location,
                    map: mapInstance,
                    icon: "/img/markers/route.svg"
                });

                infowindow.open(mapInstance, customMarker);

                customMarker.setMap(mapInstance);
            } else {
                alert("Невозможно проложить маршрут из этой точки.");
            }
        });

        return false;
    });

    $directionPopup.show();
}

$(".TransportPopup_close, .TransportPopup_overlay").on("click", function() {
    $directionPopup.hide();
});

$("#showCarRouteOnBigMapBtn").on("click", function(event) {
    event.preventDefault();
    transportMap.cleanRoute();
    $(".Transport_routes").removeClass("isOpen");
    $(".Transport_routes").hide();
    $(".Message_dialog").hide();

    bindMapRouteRenderOnClick(transportMap.map, "DRIVING");
});

$(".Transport_mobileControl-dest").on("click", function() {
    $("#showCarRouteOnBigMapBtn").trigger("click");
});

$(".Transport_mobileControl-routes").on("click", function() {
    $("#showRoutesListBtn").trigger("click");
});

$(".Transport_mainCoords").on("click", function() {
    var coords = $(this)
        .text()
        .split(", ");

    transportMap.setCenter(coords[0], coords[1]);
});

