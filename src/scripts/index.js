const slideshow = new ShadeSlider(document.querySelector(".ShadeSlider_slideshow"));

let promoSplash = new Promo(".Promo");

$(document).ready(function() {
    let wasBannerShown = window.sessionStorage.getItem("wasBannerShown");
    if (!wasBannerShown) {
        window.sessionStorage.setItem("wasBannerShown", "1");
        setTimeout(() => promoSplash.show(), 1000);
    }
});
